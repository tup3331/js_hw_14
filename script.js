const addBtn = document.querySelector("#btn-click");
const contentSection = document.querySelector("#content");

function addParagraph() {
    const newParagraph = document.createElement("p");
    newParagraph.textContent = "New Paragraph";

    contentSection.append(newParagraph);
}

addBtn.addEventListener("click", addParagraph);



////////////////////////////////////////////////////////////////////////////////////////////////



const btnInputCreate = document.createElement("button");
btnInputCreate.id = "btn-input-create";
btnInputCreate.textContent = "Button to create input";

const newSection = document.createElement("section");
newSection.style.display = "flex";
newSection.style.justifyContent = "center";
newSection.style.alignItems = "center";
newSection.style.flexDirection = "column";

contentSection.after(newSection);
newSection.append(btnInputCreate);


function inputCreate() {
    const newInput = document.createElement("input");

    newInput.setAttribute("type", "text")
    newInput.setAttribute("placeholder", "Input")

    newSection.append(newInput);
}

btnInputCreate.addEventListener("click", inputCreate);

